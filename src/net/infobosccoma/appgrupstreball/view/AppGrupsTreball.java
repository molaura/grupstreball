package net.infobosccoma.appgrupstreball.view;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Set;
import net.infobosccoma.appgrupstreball.model.Estudiant;
import net.infobosccoma.appgrupstreball.model.GrupTreball;

/**
 * Classe que conté el mètode principal de l'aplicació de gestió d'estudiants i
 * grups de treball.
 *
 * L'aplicació ha d gestionar, mitjançant una BDOO db4o, a quins grups de
 * treball pertanyen diferents estudiants dins una escola. Donat un grup de
 * treball, aquest pot tenir assignats diversos estudiants, però tot estudiant
 * només té un (però sempre un) únic grup de treball. L’aplicació ha de poder
 * fer el següent: - Donar d’alta un nou estudiant. A l’hora d’assignar-li un
 * grup, si el nom indicat no existeix, es crea un nou grup. Si existeix, a
 * l’estudiant se li assigna aquell grup. - Reassignar un estudiant a un altre
 * grup de treball. Aquest grup ja ha d’existir. Si, en fer-ho, el grup antic
 * queda sense membres, cal esborrar-lo de la BDOO. - Llistar tots els grups
 * existents. - Llistar tots els estudiants (i a quin grup pertanyen).
 *
 * Es considera que els noms dels grups i dels estudiants són únics al sistema.
 * No hi pot haver noms repetits. En base a la descripció, també cal remarcar
 * que l’única manera de crear grups nous és afegint-hi nous estudiants.
 *
 * @author Laura
 */
public class AppGrupsTreball {

    static ObjectContainer db;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        db = Db4oEmbedded.openFile("BDEstudiantsGrups.db4o");

        int opcio = -1;
        while (opcio != 0) {
            try {
                System.out.println("******************************************************************");
                System.out.println("------------------------Escull una opció: ------------------------");
                System.out.println("******************************************************************");
                System.out.println("|          1. Donar d'alta un estudiant.                         |");
                System.out.println("|          2. Reassignar un estudiant a un altre grup.           |");
                System.out.println("|          3. Mostrar tots els grups.                            |");
                System.out.println("|          4. Mostrar tots els estudiants.                       |");
                System.out.println("|          0. Sortir.                                            |");
                System.out.println("******************************************************************");
                opcio = scan.nextInt();

                switch (opcio) {
                    case 1:
                        System.out.println("Nom de l'estudiant: ");
                        String nomNouEstudiant = scan.next();
                        System.out.println("Grup al que pertany l'estudiant: ");
                        String grupNouEstudiant = scan.next();
                        System.out.println(createEstudiant(nomNouEstudiant, grupNouEstudiant));
                        break;
                    case 2:
                        System.out.println("Nom de l'estudiant: ");
                        String nomEstudiantActualitzar = scan.next();
                        System.out.println("Nou grup: ");
                        String grupNou = scan.next();
                        updateEstudiant(nomEstudiantActualitzar, grupNou);
                        break;
                    case 3:
                        listGrups();
                        break;
                    case 4:
                        listEstudiants();
                        break;
                    case 5:
                        deleteEstudiants();
                        break;
                    case 6:
                        deleteGrups();
                        break;
                    case 0:
                        break;
                    default:
                        System.out.println("Aquesta opció no és correcte");
                        break;
                }

            } catch (Exception e) {
                System.out.println("Upss! Error! No es pot accedir al programa");
            }
        }

        db.close();

    }

    static String createEstudiant(String nomNouEstudiant, String grupNouEstudiant) {
        Estudiant estudiantNou = null;

        GrupTreball grup = new GrupTreball(grupNouEstudiant, null);
        ObjectSet<GrupTreball> result = db.queryByExample(grup);
        if (result.size() >= 1) {
            grup = result.next();
        }
        estudiantNou = new Estudiant(nomNouEstudiant, grup);
        grup.sumaEstudiant();
        db.store(estudiantNou);

        // ObjectSet<Estudiant> prova = db.queryByExample(grup);
        // System.out.println(prova);
        return "S'ha creat correctement";
    }

    static void updateEstudiant(String nomEstudiant, String grupNou) {
        
        Estudiant estudiant = null;
        
        ObjectSet<Estudiant> result = db.queryByExample(nomEstudiant);

        estudiant = result.next();
        
        GrupTreball guardarGrup = estudiant.getGrupTreball();
        

        GrupTreball grup = new GrupTreball(grupNou, null);

        //Si s'ha trobat l'estudiant, se li reassigna el grup i es resta l'estudiant del grup antic.  
        if (result.size() == 1) {
            estudiant.reassignaGrup(grup);
            grup.restaEstudiant();
            //Comprova si encara hi ha estudiants assignats al grup, si no elimina el grup.
            if (guardarGrup.getNumEstudiants() <= 0) {
                db.delete(guardarGrup);
            }
            db.store(estudiant);
        } else {
            System.out.println("L'estudiant no existeix");
        }
    }

    static void listGrups() {
        List<GrupTreball> grups = db.query(GrupTreball.class);

        ListIterator<GrupTreball> lg = grups.listIterator();
        if (grups.size() >= 1) {
            while (lg.hasNext()) {
                GrupTreball element = lg.next();
                System.out.println(element);
            }
        } else {
            System.out.println("No hi ha grups a mostrar");
        }
    }

    static void listEstudiants() {
        List<Estudiant> estudiants = db.query(Estudiant.class);

        ListIterator<Estudiant> lg = estudiants.listIterator();
        while (lg.hasNext()) {
            Estudiant element = lg.next();
            System.out.println(element);
        }
    }

    static void deleteGrups() {
        List<GrupTreball> grups = db.query(GrupTreball.class);
        ListIterator<GrupTreball> lg = grups.listIterator();
        while (lg.hasNext()) {
            GrupTreball element = lg.next();
            db.delete(element);
        }
    }

    static void deleteEstudiants() {
        List<Estudiant> estudiants = db.query(Estudiant.class);

        ListIterator<Estudiant> lg = estudiants.listIterator();
        while (lg.hasNext()) {
            Estudiant element = lg.next();
            db.delete(element);
        }
    }
}
