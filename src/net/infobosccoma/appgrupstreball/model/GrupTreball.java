package net.infobosccoma.appgrupstreball.model;

/**
 * Classe que modela un grup de treball d'estudiants, que tenen un nom i treballen
 * sobre un tema en concret.
 * 
 * @author marc
 */
public class GrupTreball {
    
    //<editor-fold defaultstate="collapsed" desc="Atributs membre">
    private String nom;
    private String tema;
    private int numEstudiants = 0;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="API pública">
    
    //<editor-fold defaultstate="collapsed" desc="Constructors">
    public GrupTreball(String n, String tm) {
        nom = n;
        tema = tm;
    }
    //</editor-fold>

    
    /**
     * Incrementar la quantitat d'estudiants
     */
    public void sumaEstudiant() {
        numEstudiants++;
    }
    
    /**
     * Decrementar la quantitat d'estudiants
     */
    public void restaEstudiant() {
        numEstudiants--;
    }
    
    /**
     * Obtenir la quantitat d'estudiants
     * 
     * @return Enter que indica quants estudiants hi ha al grup
     */
    public int getNumEstudiants() {
        return numEstudiants;
    }
    
    //<editor-fold defaultstate="collapsed" desc="Mètodes sobreescrits">
    @Override
    public String toString() {
        return nom + " - " + tema;
    }
    //</editor-fold>
    
    //</editor-fold>

}
